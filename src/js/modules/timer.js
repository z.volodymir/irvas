function timer(timerSelector, deadline) {

	function getTimeRemaining(endTime) {
		const total = Date.parse(endTime) - Date.parse(new Date()),
			days = Math.floor( total / (1000 * 60 * 60 * 24)),
			hours = Math.floor(total / (1000 * 60 * 60) % 24),
			minutes = Math.floor((total / 1000 / 60) % 60),
			seconds = Math.floor((total / 1000) %  60);

		return {
			total,
			days,
			hours,
			minutes,
			seconds
		};
	}

	function getZero(num) {
		if (num >= 0 && num < 10) {
			return `0${num}`;
		}
		else {
			return num;
		}
	}

	function setTime(selector, endTime) {
		const timer = document.querySelector(selector),
			  days = document.querySelector('#days'),
			  hours = document.querySelector('#hours'),
			  minutes = document.querySelector('#minutes'),
			  seconds = document.querySelector('#seconds'),

			timeInterval = setInterval(updateTimer, 1000);

		updateTimer();

		function updateTimer() {
			const t = getTimeRemaining(endTime);

			days.textContent = getZero(t.days);
			hours.textContent = getZero(t.hours);
			minutes.textContent = getZero(t.minutes);
			seconds.textContent = getZero(t.seconds);

			if (t.total <= 0) {
				clearInterval(timeInterval);
			}
		}
	}

	setTime(timerSelector, deadline);

}

export default timer;