import checkNumInputs from "./checkNumInputs";

const forms = (state) => {

	const forms = document.querySelectorAll('form'),
		  inputs = document.querySelectorAll('input');

	checkNumInputs('input[name="user_phone"]');

	const massage = {
		loading: 'Загрузка...',
		success: 'Спасибо! Мы свяжемся с вами в ближашее время',
		failure: 'Что-то пошло не так...'
	}

	const clearInputs = () => {
		inputs.forEach(input => {
			input.value = '';
		});
	};

	const postData = async (url, data) => {
		const response = await fetch(url, {
			method: 'POST',
			body: data
		});

		return await response.text();
	};

	forms.forEach(form => {
		form.addEventListener('submit', e => {
			e.preventDefault();

			let statusMassage = document.createElement('div');
			statusMassage.textContent = massage.loading;
			statusMassage.classList.add('status');
			form.appendChild(statusMassage)

			const formData = new FormData(form);

			if (form.getAttribute('data-calc') === 'end') {
				for (let key in state) {
					formData.append(key, state[key]);
				}
			}

			postData('assets/server.php', formData)
				.then(res => {
					console.log(res);
					statusMassage.textContent = massage.success;
				})
				.catch((error) => statusMassage.textContent = massage.failure)
				.finally(() => {
					clearInputs();
					setTimeout(() => statusMassage.remove(), 10000);
				});
		});
	});
};

export default forms;