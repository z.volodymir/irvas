const modals = () => {
	function bindModal (triggerSelector, modalSelector, closeSelector, closeClickOverlay = true) {

		const triggers = document.querySelectorAll(triggerSelector),
			  modal = document.querySelector(modalSelector),
			  close = document.querySelector(closeSelector),
			  windows = document.querySelectorAll('[data-toggle]'),
			  scroll = calcScroll();

		triggers.forEach(trigger => {
			trigger.addEventListener('click', (event) => {
				if (event.target) {
					event.preventDefault();
				}
				closeWindows();

				modal.style.display = 'block';
				document.body.style.overflow = 'hidden';
				document.body.style.marginRight = `${scroll}px`;
			});
		});

		close.addEventListener('click', () => {
			closeWindows();

			modal.style.display = 'none';
			document.body.style.overflow = '';
			document.body.style.marginRight = `0px`;
		});

		modal.addEventListener('click', (event) => {
			if (event.target === modal && closeClickOverlay) {
				closeWindows();

				modal.style.display = 'none';
				document.body.style.overflow = '';
				document.body.style.marginRight = `0px`;
			}
		});

		function closeWindows () {
			windows.forEach(window => {
				window.style.display = 'none'
			});
		}

	}

	function calcScroll() {
		const div = document.createElement('div');
		div.style.width = '50px';
		div.style.height = '50px';
		div.style.overflow = 'scroll';
		div.style.visibility = 'hidden';

		document.body.appendChild(div);
		let scrollWidth = div.offsetWidth - div.clientWidth;
		div.remove();

		return scrollWidth;
	}

	function showModalByTime (selector, time) {
		setTimeout(() => {
			let display;

			document.querySelectorAll('[data-modal]').forEach(modal => {
				if (getComputedStyle(modal).display !== 'none') {
					display = 'block';
				}
			});

			if(!display) {
				document.querySelector(selector).style.display = 'block';
				document.body.style.overflow = 'hidden';
				document.body.style.marginRight = `${calcScroll()}px`;
			}
		}, time);
	}

	bindModal('.popup_engineer_btn', '.popup_engineer', '.popup_engineer .popup_close');
	bindModal('.phone_link', '.popup', '.popup_close');
	bindModal('.popup_calc_btn', '.popup_calc', '.popup_calc_close');
	bindModal('.popup_calc_button', '.popup_calc_profile', '.popup_calc_profile_close', false);
	bindModal('.popup_calc_profile_button', '.popup_calc_end', '.popup_calc_end_close', false);
	showModalByTime('.popup', 60000);
};

export default modals;