const images = () => {

	const parentSelector = document.querySelector('.works'),
		  imagePopup = document.createElement('div'),
		  bigImage = document.createElement('img');

	parentSelector.appendChild(imagePopup);
	imagePopup.appendChild(bigImage);
	imagePopup.classList.add('popup');

	imagePopup.style.cssText = `
	justify-content: center;
	align-items: center;
	display: none;
	`;

	// imagePopup.style.justifyContent = 'center';
	// imagePopup.style.alignItems = 'center';
	// imagePopup.style.display = 'none';

	parentSelector.addEventListener('click', e => {
		e.preventDefault();

		const target = e.target;
		if (target && target.classList.contains('preview')) {
			imagePopup.style.display = 'flex';
			document.body.style.overflow = 'hidden';

			const path = target.parentNode.getAttribute('href');
			bigImage.setAttribute('src', path);
		}
		if (target && target.matches('div.popup')) {
			imagePopup.style.display = 'none';
			document.body.style.overflow = 'visible';
		}
	});
};

export default images;