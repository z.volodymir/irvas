const tabs = (headerSelector, tabSelector, contentSelector, activeClass, display = 'block') => {

	const header = document.querySelector(headerSelector),
		  tabs = document.querySelectorAll(tabSelector),
		  contents = document.querySelectorAll(contentSelector);

	function showTabContent (idx = 0) {
		tabs[idx].classList.add(activeClass);
		contents[idx].style.display = display;
	}

	function hideTabContent () {
		tabs.forEach(tab => {
			tab.classList.remove(activeClass);
		});
		contents.forEach(content => {
			content.style.display = 'none';
		});
	}

	hideTabContent();
	showTabContent();

	header.addEventListener('click', (event) => {
		const target = event.target;

		if (
			target &&
			(target.classList.contains(tabSelector.replace(/\./, '')) ||
			target.parentNode.classList.contains(tabSelector.replace(/\./, '')))
		) {
			tabs.forEach((tab, idx) => {
				if (target === tab || target.parentNode === tab) {
					hideTabContent();
					showTabContent(idx);
				}
			});
		}
	});
};

export default tabs;