import checkNumInputs from "./checkNumInputs";

const changeModalState = (state) => {

	const windowForm = document.querySelectorAll('.balcon_icons_img'),
		  windowWidth = document.querySelectorAll('#width'),
		  windowHeight = document.querySelectorAll('#height'),
		  windowType = document.querySelectorAll('#view_type'),
		  windowProfile = document.querySelectorAll('.checkbox');

	checkNumInputs('#width');
	checkNumInputs('#height');

	function bindActionByElements(elements, event, prop) {
		elements.forEach((elem, idx) => {
			elem.addEventListener(event, () => {

				switch (elem.nodeName) {
					case 'SPAN':
						state[prop] = idx;
						break;
					case 'INPUT':
						if (elem.getAttribute('type') === 'checkbox') {
							idx === 0 ? state[prop] = 'cold' : state[prop] = 'hot';
							elements.forEach((elem, i) => {
								elem.checked = false;
								if (i === idx) {
									elem.checked = true;
								}
							});
						} else {
							state[prop] = elem.value;
						}
						break;
					case 'SELECT':
						state[prop] = elem.value;
						break;
				}

				console.log(state);
			});
		});
	}

	// function bindActionByElements(elements, type, prop) {
	// 	elements.forEach((elem, idx) => {
	// 		elem.addEventListener(type, () => {
	// 			if (elements.length > 1) {
	// 				state[prop] = idx;
	// 			} else {
	// 				state[prop] = elem.value;
	// 			}
	// 			console.log(state);
	// 		});
	// 	});
	// }

	bindActionByElements(windowForm, 'click', 'form');
	bindActionByElements(windowWidth, 'input', 'width');
	bindActionByElements(windowHeight, 'input', 'height');
	bindActionByElements(windowType, 'change', 'type');
	bindActionByElements(windowProfile, 'change', 'profile');

};

export default changeModalState;